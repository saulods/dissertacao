\select@language {brazil}
\contentsline {section}{\numberline {1}INTRODU\IeC {\c C}\IeC {\~A}O}{14}{section.1}
\contentsline {subsection}{\numberline {1.1}CONTEXTUALIZA\IeC {\c C}\IeC {\~A}O DO TEMA}{14}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}PROBLEMA DE PESQUISA}{15}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}OBJETIVOS}{16}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}ESTRUTURA DO TRABALHO}{17}{subsection.1.4}
\contentsline {section}{\numberline {2}WEB-CRAWLERS PARA EXTRA\IeC {\c C}\IeC {\~A}O DE DADOS NA PLATAFORMA LATTES}{18}{section.2}
\contentsline {subsection}{\numberline {2.1}WEB-CRAWLERS}{18}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}FERRAMENTAS DE EXTRA\IeC {\c C}\IeC {\~O}ES DE DADOS NA PLATAFORMA LATTES E WEB-CRAWLERS}{19}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}OntoLattes}{19}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}GeraLattes}{20}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}SemanticLattes}{20}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}ScriptLattes}{21}{subsubsection.2.2.4}
\contentsline {subsection}{\numberline {2.3}UTILIZANDO SCRIPTLATTES PARA EXTRA\IeC {\c C}\IeC {\~A}O DE CURR\IeC {\'I}CULOS E MINERA\IeC {\c C}\IeC {\~A}O DAS INFORMA\IeC {\c C}\IeC {\~O}ES}{25}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Trabalhos que utilizam o ScriptLattes}{26}{subsubsection.2.3.1}
\contentsline {section}{\numberline {3}AGRUPAMENTOS HIER\IeC {\'A}RQUICOS A PARTIR DE RELAT\IeC {\'O}RIOS GERADOS PELO SCRIPTLATTES}{29}{section.3}
\contentsline {subsection}{\numberline {3.1}AGRUPAMENTO HIER\IeC {\'A}RQUICO}{29}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}AGRUPAMENTO HIER\IeC {\'A}RQUICO DE CURR\IeC {\'I}CULOS LATTES}{30}{subsection.3.2}
\contentsline {section}{\numberline {4}APLICA\IeC {\c C}\IeC {\~O}ES}{34}{section.4}
\contentsline {subsection}{\numberline {4.1}SCRIPTSUCUPIRA}{34}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Resultados utilizando o ScriptSucupira}{38}{subsubsection.4.1.1}
\contentsline {subsection}{\numberline {4.2}SCRIPTEMEC}{54}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Resultados utilizando a ferramenta ScriptEMec}{56}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}PLANOS PARA DEFESA}{63}{section.5}
\contentsline {subsection}{\numberline {5.1}CLASSIFICA\IeC {\c C}\IeC {\~A}O DO CONCEITO CAPES}{63}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}PREDI\IeC {\c C}\IeC {\~A}O DA NOTA CAPES DE PROGRAMAS STRICTO SENSU}{64}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}CRONOGRAMA}{64}{subsection.5.3}
\contentsline {section}{REFER\IeC {\^E}NCIAS BIBLIOGR\IeC {\'A}FICAS}{68}{table.7}
